export default {
  theme: {
    extend: {
      fontFamily: {
        cairo: ["Cairo", "sans-serif"],
        poppins: ["Poppins", "sans-serif"],
      },
      colors: { mbrian: "#fd9644" },
    },
  },
};
